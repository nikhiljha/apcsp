# APCSP: Unit One Study Guide
by Nikhil Jha // September 26, 2018

## LT C-1
> I can identify the four features common to all computers, explain in general what they do, and use them to defend why certain objects are or are not computers.

- Input: Provide information to the computer. (keyboard, mouse, touch, sensors)
- Output: Get information out of the computer. (screen, speakers, vibration)
- Storage: Store information in a computer. (flash drive, hard drive, RAM)
- Processing: Change or process the provided information. (CPU, circuit)

## LT C-2
> I understand the binary and hexadecimal number system and can explain how it makes a computer work.

#### Binary

Each bit is on/off or true/false. To convert it to decimal, multiply each digit by $$2^n$$, where n is the place value of the digit, and then add them together. To convert it to hexadecimal, take each set of 4 bits and find the hexadecimal value as such...

![](https://circuitglobe.com/wp-content/uploads/2016/09/hexadecimal-to-binary-conversion-examples-3.jpg)

#### Hexadecimal

Hexadecimal is often used to represent values (numbers and memory addresses) in computer systems. Each hexadecimal digit represents 4 binary bits. To convert it to decimal, multiply each digit by $$16^n$$, where n is the place value of the digit, and then add them together.

## LT C-3
> I can determine the output of a logic gate.

There are 4 basic types of logic gates.

- AND: True iff both inputs are true.
- OR: True iff at least one input is true.
- XOR: True iff only one input is true.
- NOT: True if the input is false.

There are also inverse versions of these logic gates, which are computed the same way as the regular one, but you flip the answer.

- NAND: False iff both inputs are true.
- NOR: False iff at least one input is true.
- XNOR: False iff only one input is true.

From there, follow the arrows in the drawn logic gate to determine the output.

## LT C-4
> I can classify different features of a computer as input, processing, storage, or output AND as hardware or software.

- Inputs: Keyboard, mouse, touchscreen, gyroscope, etc.
- Outputs: Screen, lights, vibration, sound, etc.
- Processing: CPU
- Storage: HDD/SSD/Memory

Hardware: Can you physically touch it? If yes, it's hardware.
Software: If it's not hardware...

## LT C-5
> I can explain how the internet works.

- Bandwidth: maximum transmission capacity of a device
- Bit Rate: number of bits that can be sent over a given period of time
- Latency: time it takes for a bit to travel from one place to another
- IP: Internet Protocol - address of a computer
- DNS: Domain Name System - association of names to IP addresses
- Packet: information on the internet being sent
- Router: traffic managers to keep packets moving smoothly
- Fault Tolerant: network can keep sending packets, even if something goes wrong
- TCP: Transmission Control Protocol - guarantees mail services
- Scalable: service can be grown without disrupting anyone else's use of the internet
- URL: Uniform Resource Locator - code for locating sources
- Http: Hyper Text Transfer Protocol - protocol computer uses to talk to other computers
- HTML: Hyper Text Markup Language - language for deciding the content of websites
- Cookies: information stored by the browser, usually to track logins, etc

## LT C-5
> I understand the security protocols of the internet.

(From Acunetix, some random blog): HTTPS/TLS allows the connection between two mediums (client-server) to be encrypted. By encrypting the communication, we ensure that no third-party is able to read or tamper with the data that is being exchanged on our connection with the server. An unencrypted communication could and would expose sensitive data such as usernames, passwords, credit card numbers and generally anything that is being sent back and forth during the connection.

Using a normal – unencrypted – connection, if a third party intercepted our connection with the server, they would be able to see information exchanged in plaintext (human readable format). If for example we access our website’s administration panel without SSL and someone is sniffing the local network’s traffic, they would be able to see the following.

![](https://d3eaqdewfg2crq.cloudfront.net/wp-content/uploads/2017/01/image32.png)

The cookie which we use to authenticate on our website is now sent in plaintext and anyone intercepting the connection can see it. This means that an attacker can use this information to login on our website’s administration panel. From then on the attacker’s options expand dramatically for data leak or further exploitation.

However, if we access our website using SSL/TLS, they would see something quite different.

![](https://d3eaqdewfg2crq.cloudfront.net/wp-content/uploads/2017/01/image04-1.png)

In this case, the information is useless to the attacker.

## LT R-1
> I understand basic coding vocabulary.

- Algorithm: An algorithm is a step by step list of instructions that if followed exactly will solve the problem under consideration.
- Program: A list of instructions that specifies how to perform a computation.
- Input: self-explanatory
- Output: self-explanatory
- Debugging: The process of finding and removing any of the three kinds of programming errors. (syntax errors, runtime errors, and semantic errors)
- Comments: text that is intended only for the human reader - it is completely ignored by the interpreter
  - Written in Python as a “#”.
  ```python
  # This is a comment. Anything I type here is irrelevant.
  ```


## LT R-2
> I can use the print function in python.

```python
# To print, do this.
print("something")
```

- Case sensitive.
- Two sets of quotes -> print exactly.
- No quotes -> prints the variable with that name.
- Numbers -> prints the number.
- Symbols work. The `\` symbol does not, or sometimes it does weird things (probably don’t need to know this.)


## LT R-3
> I can identify the data types in a coding language and the conversions in python.

- String: “has quotes around it”
  - The input() function returns a string.
- Integer: A number without a decimal
- Float: A number with a decimal.
    - Note that 1.0 + 1.0 = 2.0, not 2.
- Character: A single letter or symbol. Not sure if you need to know this, but a backslash followed by another character is still one character.

## LT R-4
> I can assign variables to different data types in Python.

There is one way to assign things (that you need to know.)

```python
variable = "something"
print(variable) # prints: something
```

## LT R-5
> I can use mathematical operations and expressions in Python.

PEMDAS: First evaluate parenthesis, then exponents, then multiplication, then division, then addition, then subtraction.

If there are multiple exponents, evaluate them from right to left. In effect though it really doesn't matter, because math.

## LT R-6
> I can receive input from a user in Python.

```
variable = input("Do you like pie? ")
```

**Remember:** The input function returns a *string*. If you need an integer or a float, cast with the int() or float() functions.

## LT R-7
> I understand how debugging is a critical process when writing python programming.

Check like this:

1. Check syntax. This means non-matching parenthesis, misspellings, etc.
2. Check types. Make sure you don't try to add strings.
3. Check names. Did you type the correct variable names in the correct place?
4. Check logic. "Run" the code in your head. Did it do what you expected?

Of course, in the real world you'll only need to do #4 (and mabye #3), because the other ones will be automatically taken care of for you. Unfortunately, APCSP is not the real world.




