# Etch-A-Sketch Python
# Nikhil Jha / 4 Oct 2018
# v0.0.1

# A simple turtle graphics demo.

# Import turtle library.
import turtle

# Turtle and screen objects.
window = turtle.Screen()
jack = turtle.Turtle()

# Control functions.
def up():
    jack.seth(90)
    jack.forward(10)

def down():
    jack.seth(270)
    jack.forward(10)

def left():
    jack.seth(180)
    jack.forward(10)

def right():
    jack.seth(360)
    jack.forward(10)

# Key mappings.
window.onkey(up, "Up")
window.onkey(down, "Down")
window.onkey(left, "Left")
window.onkey(right, "Right")
window.listen()

# Prevent the program from exiting.
turtle.mainloop()
